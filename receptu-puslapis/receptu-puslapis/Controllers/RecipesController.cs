﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using receptu_puslapis.Models;
using System.Web.Security;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Google;

namespace receptu_puslapis.Controllers
{
    public class RecipesController : Controller
    {
        RecipiesDbContext db = new RecipiesDbContext();

        public ActionResult Index()
        {
            return RedirectToAction("AllRecipes");
        }

        [HttpGet]
        public ActionResult AllRecipes()
        {
            var recipes = db.Recipes.Where(x => x.state == RecipeState.Approved).ToList();
            TempData["new_recipes"] = db.Recipes.ToList().Count - recipes.Count;
            return View(recipes);
        }

        [HttpGet]
        [Authorize]
        public ActionResult CreateRecipeView()
        {
            TestData01 testData01 = new TestData01();
            testData01.ingredients = db.Ingredients.Where(x => x.state == IngredientState.Approved).ToList();
            testData01.recipes = db.Recipes.ToList();
            return View(testData01);
        }

        [HttpPost]
        [Authorize]
        public ActionResult CreateRecipeView(string title, string description, RecipeDifficulty difficulty, int prepTime, string[] ingredients)
        {
            var dateTimeNow = DateTime.Now;
            var recipe = new Recipe()
            {
                title = title,
                description = description,
                uploadDate = dateTimeNow,
                updateDate = dateTimeNow,
                approvedDate = dateTimeNow,
                prepTime = prepTime,
                difficulty = difficulty,
                state = RecipeState.Pending
            };
            db.Recipes.Add(recipe);
            if (ingredients != null)
            {
                for (int i = 0; i < ingredients.Length; ++i)
                {
                    string ingredientName = ingredients[i];
                    var ingredient = db.Ingredients.First(x => x.name == ingredientName);
                    if (ingredient != null)
                    {
                        recipe.Ingredients.Add(ingredient);
                        //ingredient.Recipes.
                        //db.Ingredients.Attach(ingredient);
                        //recipe.Ingredients.Add(ingredient);
                        //ingredient.Recipes.Add(recipe);
                    }
                }
            }
            db.SaveChanges();
            TempData["success"] = "Recipe successfully created!";
            //Response.Write("[title=" + title + "][description=" + description + "]");
            return RedirectToAction("AllRecipes");
        }

        [HttpGet]
        [Authorize]
        public ActionResult ViewNewRecipes()
        {
            bool webManager = User.IsInRole("Moderator") || User.IsInRole("Admin");
            if (!webManager)
            {
                return RedirectToAction("AllRecipes");
            }
            var notApprovedRecipes = db.Recipes.Where(x => x.state != RecipeState.Approved)
                .OrderBy(x => x.uploadDate).ToList();

            //Response.Write(notApprovedRecipes.Count > 0 ? "[The list is not empty]" : "[The list is empty]");
            return View(notApprovedRecipes);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ViewRecipe(int? id)
        {
            if (TempData["id"] != null)
            {
                id = (int)TempData["id"];
            }
            if (id == null)
            {
                return RedirectToAction("AllRecipes");
            }

            var recipe = db.Recipes.First(x => x.id == id);
            if (recipe == null)
            {
                return RedirectToAction("AllRecipes");
            }

            var app = new ApplicationDbContext();
            var users = app.Users.ToList();
            var comments = db.Comments.Where(x => x.Recipe.id == recipe.id).ToList();
            var ratings = db.Ratings.Where(x => x.RecipeId == recipe.id).ToList();

            double rating = 0.0;
            for (int i = 0; i < ratings.Count; ++i)
            {
                rating += ratings[i].value;
            }
            rating /= (double)(ratings.Count);

            var model = new ViewRecipeModel()
            {
                recipe = recipe,
                comments = comments,
                users = users,
                rating = rating,
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult PostComment(int id, string comment)
        {
            var Comment = new Comment()
            {
                text = comment,
                RecipeId = id,
                commentDate = DateTime.Now,
                userId = User.Identity.GetUserId()
            };
            db.Comments.Add(Comment);
            db.SaveChanges();
            TempData["id"] = id;

            var classifier = CommentClassifier.Create();
            TempData["comment_type"] = classifier.AnalyzeComment(comment);

            return RedirectToAction("ViewRecipe", id);
        }

        [HttpPost]
        [Authorize]
        public ActionResult PostRating(int id, double rate)
        {
            var userId = User.Identity.GetUserId();
            var recipeRatings = db.Ratings.Where(x => x.RecipeId == id);
            var currentUserRating = recipeRatings.FirstOrDefault(x => x.userId == userId);
            if (currentUserRating != null)
            {
                currentUserRating.value = rate;
            }
            else
            {
                currentUserRating = new Rating()
                {
                    value = rate,
                    RecipeId = id,
                    userId = userId,
                };
                db.Ratings.Add(currentUserRating);
            }
            TempData["id"] = id;
            db.SaveChanges();
            return RedirectToAction("ViewRecipe", id);
        }

        [HttpPost]
        [Authorize]
        public ActionResult EditRecipe(int id, string state)
        {
            TempData["wants_to_edit"] = true;
            TempData["id"] = id;
            if (state != null && state.Length > 0)
            {
                TempData["state"] = true;
            }
            return RedirectToAction("ViewRecipe", id);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangeRecipeState(int id, string button)
        {
            var recipe = db.Recipes.First(x => x.id == id);

            if (recipe == null)
            {
                return RedirectToAction("AllRecipes");
            }

            if (button == "Approved")
            {
                recipe.state = RecipeState.Approved;
            }
            else if (button == "Needs update")
            {
                recipe.state = RecipeState.NeedsUpdate;
            }
            else if (button == "Decline")
            {
                recipe.state = RecipeState.Declined;
            }
            else
            {
                return RedirectToAction("AllRecipes");
            }
            recipe.approvedDate = DateTime.Now;
            db.SaveChanges();
            return RedirectToAction("AllRecipes");
        }
    }
}