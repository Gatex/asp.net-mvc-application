﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using receptu_puslapis.Models;

namespace receptu_puslapis.Controllers
{
    public class IngredientsController : Controller
    {
        RecipiesDbContext db = new RecipiesDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            return RedirectToAction("AllIngredients");
        }

        [HttpGet]
        public ActionResult AllIngredients()
        {
            var ingredients = db.Ingredients.Where(x => x.state == IngredientState.Approved).OrderBy(x => x.name);
            TempData["new_ingredients"] = db.Ingredients.ToList().Count - ingredients.ToList().Count;
            return View(ingredients);
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult SuggestNewIngredient()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult ViewNewIngredients()
        {
            var ingredients = db.Ingredients.Where(x => x.state != IngredientState.Approved).OrderByDescending(x => x.numberOfTimesUsed);
            return View(ingredients);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SuggestNewIngredient(string ingredient)
        {
            ingredient = ingredient.Trim();
            if (ingredient.Length == 0)
            {
                TempData["error"] = "Ingredient name is too short.";
                return View();
            }
            if (!db.Ingredients.Any(x => x.name == ingredient))
            {
                var ingred = new Ingredient();
                ingred.name = ingredient;
                ingred.numberOfTimesUsed = 1;
                ingred.state = IngredientState.Pending;
                db.Ingredients.Add(ingred);
            }
            else
            {
                var ingred = db.Ingredients.First(x => x.name == ingredient);
                if (ingred != null)
                {
                    ingred.numberOfTimesUsed++;
                }
            }
            db.SaveChanges();
            TempData["success"] = "Ingredient successfully suggested!";
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangeState(int id, string button)
        {
            var ingredient = db.Ingredients.First(x => x.id == id);
            if (ingredient == null)
            {
                return RedirectToAction("ViewNewIngredients");
            }

            if (button == "Approve")
            {
                ingredient.state = IngredientState.Approved;
                TempData["state_changed"] = "Approved";
                db.SaveChanges();
            }
            else if (button == "Decline")
            {
                ingredient.state = IngredientState.Declined;
                TempData["state_changed"] = "Declined";
                db.SaveChanges();
            }
            return RedirectToAction("ViewNewIngredients");
        }

        [HttpPost]
        [Authorize]
        public ActionResult ViewNewIngredients(IngredientState state, int id)
        {
            foreach (var item in db.Ingredients)
            {
                if (item.id == id)
                {
                    item.state = state;
                }
            }
            db.SaveChanges();
            if(id == 1)
            {
                return RedirectToAction("AllIngredients");
            }
            else
            {
                return RedirectToAction("ViewNewIngredients");
            }
        }

    }
}