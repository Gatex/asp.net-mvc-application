//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace receptu_puslapis.Models
{
    using System;
    
    public enum RecipeDifficulty : int
    {
        Easy = 0,
        Medium = 1,
        Advanced = 2,
        Chief = 3
    }
}
