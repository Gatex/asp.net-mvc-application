
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/29/2018 01:14:48
-- Generated from EDMX file: D:\Projects\bitbucket-receptai\receptu-puslapis\receptu-puslapis\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [RecipeDbContext];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_RecipeIngredient_Recipe]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RecipeIngredient] DROP CONSTRAINT [FK_RecipeIngredient_Recipe];
GO
IF OBJECT_ID(N'[dbo].[FK_RecipeIngredient_Ingredient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RecipeIngredient] DROP CONSTRAINT [FK_RecipeIngredient_Ingredient];
GO
IF OBJECT_ID(N'[dbo].[FK_RecipeRating]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Ratings] DROP CONSTRAINT [FK_RecipeRating];
GO
IF OBJECT_ID(N'[dbo].[FK_RecipePhoto]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Photos] DROP CONSTRAINT [FK_RecipePhoto];
GO
IF OBJECT_ID(N'[dbo].[FK_RecipeComment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_RecipeComment];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Comments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comments];
GO
IF OBJECT_ID(N'[dbo].[Ingredients]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Ingredients];
GO
IF OBJECT_ID(N'[dbo].[Photos]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Photos];
GO
IF OBJECT_ID(N'[dbo].[Recipes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Recipes];
GO
IF OBJECT_ID(N'[dbo].[Ratings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Ratings];
GO
IF OBJECT_ID(N'[dbo].[RecipeIngredient]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RecipeIngredient];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [id] int IDENTITY(1,1) NOT NULL,
    [text] nvarchar(max)  NULL,
    [commentDate] datetime  NOT NULL,
    [userId] nvarchar(max)  NULL,
    [RecipeId] int  NOT NULL
);
GO

-- Creating table 'Ingredients'
CREATE TABLE [dbo].[Ingredients] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [state] int  NOT NULL,
    [numberOfTimesUsed] int  NOT NULL
);
GO

-- Creating table 'Photos'
CREATE TABLE [dbo].[Photos] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NULL,
    [path] nvarchar(max)  NULL,
    [RecipeId] int  NOT NULL
);
GO

-- Creating table 'Recipes'
CREATE TABLE [dbo].[Recipes] (
    [id] int IDENTITY(1,1) NOT NULL,
    [title] nvarchar(max)  NULL,
    [description] nvarchar(max)  NULL,
    [uploadDate] datetime  NOT NULL,
    [updateDate] datetime  NOT NULL,
    [prepTime] int  NOT NULL,
    [difficulty] int  NOT NULL,
    [state] int  NULL,
    [approvedDate] datetime  NOT NULL
);
GO

-- Creating table 'Ratings'
CREATE TABLE [dbo].[Ratings] (
    [id] int IDENTITY(1,1) NOT NULL,
    [value] float  NOT NULL,
    [userId] nvarchar(max)  NOT NULL,
    [RecipeId] int  NOT NULL
);
GO

-- Creating table 'RecipeIngredients'
CREATE TABLE [dbo].[RecipeIngredients] (
    [Recipes_id] int  NOT NULL,
    [Ingredients_id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Ingredients'
ALTER TABLE [dbo].[Ingredients]
ADD CONSTRAINT [PK_Ingredients]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Photos'
ALTER TABLE [dbo].[Photos]
ADD CONSTRAINT [PK_Photos]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Recipes'
ALTER TABLE [dbo].[Recipes]
ADD CONSTRAINT [PK_Recipes]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Ratings'
ALTER TABLE [dbo].[Ratings]
ADD CONSTRAINT [PK_Ratings]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [Recipes_id], [Ingredients_id] in table 'RecipeIngredients'
ALTER TABLE [dbo].[RecipeIngredients]
ADD CONSTRAINT [PK_RecipeIngredients]
    PRIMARY KEY CLUSTERED ([Recipes_id], [Ingredients_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Recipes_id] in table 'RecipeIngredients'
ALTER TABLE [dbo].[RecipeIngredients]
ADD CONSTRAINT [FK_RecipeIngredient_Recipe]
    FOREIGN KEY ([Recipes_id])
    REFERENCES [dbo].[Recipes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Ingredients_id] in table 'RecipeIngredients'
ALTER TABLE [dbo].[RecipeIngredients]
ADD CONSTRAINT [FK_RecipeIngredient_Ingredient]
    FOREIGN KEY ([Ingredients_id])
    REFERENCES [dbo].[Ingredients]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RecipeIngredient_Ingredient'
CREATE INDEX [IX_FK_RecipeIngredient_Ingredient]
ON [dbo].[RecipeIngredients]
    ([Ingredients_id]);
GO

-- Creating foreign key on [RecipeId] in table 'Ratings'
ALTER TABLE [dbo].[Ratings]
ADD CONSTRAINT [FK_RecipeRating]
    FOREIGN KEY ([RecipeId])
    REFERENCES [dbo].[Recipes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RecipeRating'
CREATE INDEX [IX_FK_RecipeRating]
ON [dbo].[Ratings]
    ([RecipeId]);
GO

-- Creating foreign key on [RecipeId] in table 'Photos'
ALTER TABLE [dbo].[Photos]
ADD CONSTRAINT [FK_RecipePhoto]
    FOREIGN KEY ([RecipeId])
    REFERENCES [dbo].[Recipes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RecipePhoto'
CREATE INDEX [IX_FK_RecipePhoto]
ON [dbo].[Photos]
    ([RecipeId]);
GO

-- Creating foreign key on [RecipeId] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_RecipeComment]
    FOREIGN KEY ([RecipeId])
    REFERENCES [dbo].[Recipes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RecipeComment'
CREATE INDEX [IX_FK_RecipeComment]
ON [dbo].[Comments]
    ([RecipeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------