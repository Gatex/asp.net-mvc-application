﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace receptu_puslapis.Models
{
    public class Contest
    {
        public int id { get; set; }
        public int sponsorId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime announcementDate { get; set; }
        public DateTime participationDeadline { get; set; }
        public DateTime endingDate { get; set; }
        public bool active { get; set; }
        public int[] participantsIds { get; set; }
    }
}