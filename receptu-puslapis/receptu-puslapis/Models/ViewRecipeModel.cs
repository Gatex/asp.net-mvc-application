﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace receptu_puslapis.Models
{
    public class ViewRecipeModel
    {
        public Recipe recipe { get; set; }
        public List<Comment> comments { get; set; }
        public List<ApplicationUser> users { get; set; }
        public double rating { get; set; }
    }
}