﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace receptu_puslapis.Models
{
    public class TestData01
    {
        public RecipeDifficulty difficulty;
        public List<Recipe> recipes { get; set; }
        public List<Ingredient> ingredients { get; set; }
    }
}