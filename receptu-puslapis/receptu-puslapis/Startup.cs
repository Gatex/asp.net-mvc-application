﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(receptu_puslapis.Startup))]
namespace receptu_puslapis
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
