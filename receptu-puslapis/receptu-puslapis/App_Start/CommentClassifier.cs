﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace receptu_puslapis
{
    public class CommentClassifier
    {
        public string splitters = " |\\~|\\,|\\>|\\.|\\-|\\+|\\/|\\<|\\?|\\;|\\:|\\[|\\]|\\}|\\{|\\!|\\@|\\#|\\%|\\^|\\&|\\*|\\)|\\(|\\_|\\=";
        public const string folderPathAuto = "C:\\Users\\Gabrielius\\Desktop\\Stuff\\Projektai\\CommentsClassificator\\Auto";
        public const string folderPathIT = "C:\\Users\\Gabrielius\\Desktop\\Stuff\\Projektai\\CommentsClassificator\\IT";
        public const string folderPathRecipes = "C:\\Users\\Gabrielius\\Desktop\\Stuff\\Projektai\\CommentsClassificator\\Recipes";

        public int totalAutoWords = 0;
        public int totalITWords = 0;
        public int totalRecipesWords = 0;

        public Dictionary<string, double> AutoHT_PSW = new Dictionary<string, double>();
        public Dictionary<string, double> IT_HT_PSW = new Dictionary<string, double>();
        public Dictionary<string, double> RecipesHT_PSW = new Dictionary<string, double>();

        //kad atskirt reiksmes 
        public int p_true = -1;
        public int p_false = -2;

        public CommentClassifier()
        {
            Dictionary<string, int> AutoHT = new Dictionary<string, int>();
            Dictionary<string, int> IT_HT = new Dictionary<string, int>();
            Dictionary<string, int> RecipesHT = new Dictionary<string, int>();

            AutoHT = CreateDictionary(folderPathAuto);
            IT_HT = CreateDictionary(folderPathIT);
            RecipesHT = CreateDictionary(folderPathRecipes);

            Dictionary<string, int> JoinedDictsForAuto = new Dictionary<string, int>();
            Dictionary<string, int> JoinedDictsForIT = new Dictionary<string, int>();
            Dictionary<string, int> JoinedDictsForRecipes = new Dictionary<string, int>();

            JoinedDictsForAuto = JoinDictionaries(IT_HT, RecipesHT);
            JoinedDictsForIT = JoinDictionaries(AutoHT, RecipesHT);
            JoinedDictsForRecipes = JoinDictionaries(AutoHT, IT_HT);

            FindDifferentWords(AutoHT, JoinedDictsForAuto);
            FindDifferentWords(IT_HT, JoinedDictsForIT);
            FindDifferentWords(RecipesHT, JoinedDictsForRecipes);


            Dictionary<string, int> AutoHT_AndOthers = new Dictionary<string, int>();
            Dictionary<string, int> IT_HT_AndOthers = new Dictionary<string, int>();
            Dictionary<string, int> RecipesHT_AndOthers = new Dictionary<string, int>();

            Dictionary<string, double> AutoHT_PWS = new Dictionary<string, double>();
            Dictionary<string, double> IT_PWS = new Dictionary<string, double>();
            Dictionary<string, double> RecipesHT_PWS = new Dictionary<string, double>();

            Dictionary<string, double> AutoHT_PWH = new Dictionary<string, double>();
            Dictionary<string, double> IT_PWH = new Dictionary<string, double>();
            Dictionary<string, double> RecipesHT_PWH = new Dictionary<string, double>();



            AutoHT_AndOthers = CommonWordsFromDictionaries(AutoHT, JoinedDictsForAuto);
            IT_HT_AndOthers = CommonWordsFromDictionaries(IT_HT, JoinedDictsForIT);
            RecipesHT_AndOthers = CommonWordsFromDictionaries(RecipesHT, JoinedDictsForRecipes);

            AutoHT_PWS = CalculatePWSandPWH(AutoHT, totalAutoWords);
            IT_PWS = CalculatePWSandPWH(IT_HT, totalITWords);
            RecipesHT_PWS = CalculatePWSandPWH(RecipesHT, totalRecipesWords);

            AutoHT_PWH = CalculatePWSandPWH(AutoHT_AndOthers, totalITWords + totalRecipesWords);
            IT_PWH = CalculatePWSandPWH(IT_HT_AndOthers, totalAutoWords + totalRecipesWords);
            RecipesHT_PWH = CalculatePWSandPWH(RecipesHT_AndOthers, totalITWords + totalAutoWords);

            AutoHT_PSW = CalculatePSW(AutoHT_PWS, AutoHT_PWH);
            IT_HT_PSW = CalculatePSW(IT_PWS, IT_PWH);
            RecipesHT_PSW = CalculatePSW(RecipesHT_PWS, RecipesHT_PWH);
        }

        public static CommentClassifier Create()
        {
            return new CommentClassifier();
        }

        public Dictionary<string, int> CreateDictionary(string folderPath)
        {

            Dictionary<string, int> dict = new Dictionary<string, int>();

            foreach (string inputFile in Directory.GetFiles(@folderPath, "*.txt"))
            {
                using (StreamReader fd = new StreamReader(inputFile))
                {
                    String line = fd.ReadLine();

                    while (line != null)
                    {
                        string[] parts = line.Split(splitters.ToCharArray());
                        foreach (string part in parts)
                        {
                            if (StringValidation(part, 4))
                            {
                                string tempPart = ChangeString(part);
                                if (folderPath.IndexOf("Auto") != -1)
                                    totalAutoWords++;
                                else if (folderPath.IndexOf("Rec") != -1)
                                    totalRecipesWords++;
                                else
                                    totalITWords++;

                                if (dict.ContainsKey(tempPart))
                                    dict[tempPart] = dict[tempPart] + 1;
                                else
                                    dict.Add(tempPart, 1);
                            }
                        }
                        line = fd.ReadLine();
                    }
                }
            }
            return dict;
        }

        public Dictionary<string, int> CommonWordsFromDictionaries(Dictionary<string, int> currentDict, Dictionary<string, int> otherDictionaries)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();

            foreach (string key in currentDict.Keys)
            {
                if (otherDictionaries.ContainsKey(key))
                    dict[key] = otherDictionaries[key];
            }
            return dict;
        }

        public Dictionary<string, double> CalculatePWSandPWH(Dictionary<string, int> dictionary, int totalWordsCount)
        {
            Dictionary<string, double> dict = new Dictionary<string, double>();
            foreach (string key in dictionary.Keys)
            {
                if ((int)dictionary[key] != p_false && (int)dictionary[key] != p_true)
                {
                    double value = ((double)dictionary[key] / (double)totalWordsCount);
                    dict.Add(key, value);
                }
                else if ((int)dictionary[key] == p_false)
                    dict.Add(key, 0.01);
                else
                    dict.Add(key, 0.99);

            }
            return dict;
        }

        public Dictionary<string, double> CalculatePSW(Dictionary<string, double> PWS, Dictionary<string, double> PWH)
        {
            Dictionary<string, double> dict = new Dictionary<string, double>();

            foreach (string key in PWS.Keys)
            {
                if (PWH.ContainsKey(key))
                {
                    double value = (double)PWS[key] / (PWS[key] + PWH[key]);
                    dict.Add(key, value);
                }

            }

            return dict;
        }

        public Dictionary<string, int> JoinDictionaries(Dictionary<string, int> dictonary1, Dictionary<string, int> dictonary2)
        {
            Dictionary<string, int> dict = new Dictionary<string, int>(dictonary1);
            Dictionary<string, int> dict2 = new Dictionary<string, int>(dictonary2);

            foreach (string key in dict2.Keys)
            {
                if (dict.ContainsKey(key))
                {
                    dict[key] += dict2[key];
                }
                else
                    dict.Add(key, dict2[key]);
            }
            return dict;
        }

        public bool StringValidation(string word, int allowedMinLength)
        {
            if (word.Length <= allowedMinLength)
                return false;

            return true;
        }

        public string ChangeString(string word)
        {
            string loweredWord = word.ToLower();
            string correctWord = "";

            foreach (char ch in loweredWord)
            {
                char simb;
                switch (ch)
                {
                    case 'ą':
                        {
                            simb = 'a';
                            break;
                        }
                    case 'č':
                        {
                            simb = 'c';
                            break;
                        }
                    case 'ę':
                    case 'ė':
                        {
                            simb = 'e';
                            break;
                        }
                    case 'į':
                        {
                            simb = 'i';
                            break;
                        }
                    case 'š':
                        {
                            simb = 's';
                            break;
                        }
                    case 'ų':
                    case 'ū':
                        {
                            simb = 'u';
                            break;
                        }
                    case 'ž':
                        {
                            simb = 'z';
                            break;
                        }
                    default:
                        {
                            simb = ch;
                            break;
                        }

                }
                correctWord += simb;
            }
            return correctWord;
        }

        public string AnalyzeComment(string comment)
        {
            Dictionary<string, double> TestAgainstAutoPSW = TestAgainstPSW(comment, AutoHT_PSW);
            Dictionary<string, double> TestAgainstITPSW = TestAgainstPSW(comment, IT_HT_PSW);
            Dictionary<string, double> TestAgainstRexipesPSW = TestAgainstPSW(comment, RecipesHT_PSW);

            double autoProbability = CalculateProbability(TestAgainstAutoPSW);
            double itProbability = CalculateProbability(TestAgainstITPSW);
            double recipesProbability = CalculateProbability(TestAgainstRexipesPSW);

            //Console.WriteLine("Auto Probability: " + autoProbability);
            //Console.WriteLine("IT Probability: " + itProbability);
            //Console.WriteLine("Recipes Probability: " + recipesProbability);

            string ret = "{0}, p(recipe)={1:0.00000} p(auto)={2:0.00000} p(it)={3:0.00000}";

            if (autoProbability > itProbability && autoProbability > recipesProbability)
                return string.Format(ret, "Autos", recipesProbability, autoProbability, itProbability);
            //return "Autos, probability: " + autoProbability;
            //return "Komentaro klasifikacija: automobiliai";
            else if (itProbability > autoProbability && itProbability > recipesProbability)
                return string.Format(ret, "IT", recipesProbability, autoProbability, itProbability);
                //return "IT, probability: " + itProbability;
            //return "Komentaro klasifikacija: IT";
            else if (recipesProbability > autoProbability && recipesProbability > itProbability)
                return string.Format(ret, "Recipes", recipesProbability, autoProbability, itProbability);
                //return "Recipes, probability: " + recipesProbability;
            //return "Komentaro klasfikacija: receptai";
            else
                return string.Format(ret, "Not enough data!", recipesProbability, autoProbability, itProbability);
                //return "Not enough data!";
        }

        public Dictionary<string, double> TestAgainstPSW(string comment, Dictionary<string, double> PSW)
        {
            Dictionary<string, double> dict = new Dictionary<string, double>();

            string[] words = comment.Split(splitters.ToCharArray());
            foreach (string word in words)
            {
                if (StringValidation(word, 4))
                {
                    string tempPart = ChangeString(word);
                    if (PSW.ContainsKey(tempPart))
                        dict[tempPart] = PSW[tempPart];
                    else
                        dict.Add(tempPart, 0.4);
                }
            }
            return dict;
        }

        public double CalculateProbability(Dictionary<string, double> dictionary)
        {
            double numerator = 1;
            double denominator = 1;

            foreach (string key in dictionary.Keys)
            {
                numerator += (double)dictionary[key];
                denominator += (double)(1 - dictionary[key]);
            }

            return numerator / (numerator + denominator);
        }

        public void FindDifferentWords(Dictionary<string, int> mainDict, Dictionary<string, int> otherDicts)
        {
            Dictionary<string, int> tempDict = new Dictionary<string, int>(mainDict);
            Dictionary<string, int> tempDict2 = new Dictionary<string, int>(otherDicts);

            foreach (string key in tempDict.Keys)
            {
                if (!otherDicts.ContainsKey(key))
                {
                    otherDicts.Add(key, p_false);
                    mainDict[key] = p_true;
                }

            }

            foreach (string key in tempDict2.Keys)
            {
                if (!mainDict.ContainsKey(key))
                {
                    mainDict.Add(key, p_false);
                    otherDicts[key] = p_true;
                }

            }
        }
    }
}
