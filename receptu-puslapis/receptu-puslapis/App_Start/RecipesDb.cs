﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using receptu_puslapis.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace receptu_puslapis
{
    public class RecipiesDbContext : DbContext
    {
        public RecipiesDbContext() : base("RecipeDbContext")
        {
        }

        public static RecipiesDbContext Create()
        {
            return new RecipiesDbContext();
        }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Recipe>().HasMany(r => r.Ingredients).WithMany(i => i.Recipes);
        }
    }

}
